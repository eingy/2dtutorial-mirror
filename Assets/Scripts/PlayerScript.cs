﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Player controller and behavior
/// </summary>
public class PlayerScript : MonoBehaviour {
	// Speed of the ship. Making this public allows it to be accessed & modifid via the Unity Inspector.
	public Vector2 speed = new Vector2(10, 10);

	// Store the movement
	private Vector2 movement;

	// Use this for initialization
	void Start() {
	
	}
	
	// Update is called once per frame
	void Update() {
		// Retrieve axis information
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");

		// Movement per direction
		movement = new Vector2(speed.x * inputX, speed.y * inputY);
	}

	// This is called on a fixed schedule. Anything related to physics should be done here. 
	void FixedUpdate() {
		// Move the game object;
		rigidbody2D.velocity = movement;
	}
}
